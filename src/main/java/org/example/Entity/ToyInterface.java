package main.java.org.example.Entity;

public interface ToyInterface {
  int getId();
  String getName();
  int getQuantity();
  String getDescription();
}
