package main.java.org.example.Entity;

public class Toy implements ToyInterface {

  private int id;
  private String name;
  private int quantity;
  private String description;

  public Toy(int id, String name, int quantity, String description) {
    this.id = id;
    this.name = name;
    this.quantity = quantity;
    this.description = description;
  }

  public int getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public int getQuantity() {
    return quantity;
  }

  public String getDescription() {
    return description;
  }

  @Override
  public String toString() {
    return (
      "Toy{" +
      "id=" +
      id +
      ", name='" +
      name +
      '\'' +
      ", quantity=" +
      quantity +
      ", description='" +
      description +
      '\'' +
      '}'
    );
  }
}
