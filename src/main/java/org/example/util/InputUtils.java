package main.java.org.example.util;

import java.util.Scanner;


public class InputUtils {

    private static final Scanner scanner = new Scanner(System.in);

    public static int getIntInput() {
        while (!scanner.hasNextInt()) {
            scanner.next();
        }
        return scanner.nextInt();
    }

    public static String getStringInput() {
        return scanner.next();
    }
}