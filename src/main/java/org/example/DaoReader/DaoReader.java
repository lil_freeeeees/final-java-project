package main.java.org.example.DaoReader;

import main.java.org.example.Entity.Toy;

import java.io.IOException;
import java.util.List;

public interface DaoReader {

    List<Toy> readToysFromFile() throws IOException;
}

