package main.java.org.example.DaoReader;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import main.java.org.example.Entity.AdminToy;
import main.java.org.example.Entity.Toy;

public class AdminDaoReader extends DaoReaderImpl {

  private final String filePath;

  public AdminDaoReader(String filePath) {
    super(filePath);
    this.filePath = filePath;
  }

  // Example of reading admin changes in DaoReaderImpl
  public List<AdminToy> readAdminChangesFromFile()
    throws FileNotFoundException {
    List<AdminToy> adminToys = new ArrayList<>();
    try {
      Scanner scanner = new Scanner(new File(filePath));
      while (scanner.hasNext()) {
        String line = scanner.nextLine();
        String[] tokens = line.split(";");
        if (tokens.length == 5 && tokens[4].equals("ADMIN")) {
          int id = Integer.parseInt(tokens[0]);
          String name = tokens[1];
          int quantity = Integer.parseInt(tokens[2]);
          String description = tokens[3];
          AdminToy adminToy = new AdminToy(id, name, quantity, description);
          adminToys.add(adminToy);
        }
      }
      scanner.close();
    } catch (FileNotFoundException e) {
      System.err.println("File not found: " + filePath);
      throw e;
    }
    return adminToys;
  }

  // New method to update the database with admin-specific changes
  public void updateDatabase(List<AdminToy> adminToys) throws IOException {
    Files.write(Paths.get(getFilePath()), "".getBytes());

    try (FileWriter writer = new FileWriter(new File(getFilePath()), true)) {
      for (AdminToy toy : adminToys) {
        writer.write(
          String.format(
            "%d;%s;%d;%s%n",
            toy.getId(),
            toy.getName(),
            toy.getQuantity(),
            toy.getDescription()
          )
        );
      }
    }
  }

  public void addToyToFile(AdminToy adminToy) throws IOException {
    System.out.println("ADD");
    try (FileWriter writer = new FileWriter(new File(filePath), true)) {
      writer.write(
        String.format(
          "%d;%s;%d;%s;ADMIN%n",
          adminToy.getId(),
          adminToy.getName(),
          adminToy.getQuantity(),
          adminToy.getDescription()
        )
      );
    }
  }
}
