package main.java.org.example.DaoReader;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import main.java.org.example.Entity.AdminToy;
import main.java.org.example.Entity.Toy;
import main.java.org.example.Entity.ToyInterface;

public class DaoReaderImpl implements DaoReader {

  private final String filePath;

  public DaoReaderImpl(String filePath) {
    this.filePath = filePath;
  }

  public String getFilePath() {
    return filePath;
  }

  @Override
  public List<Toy> readToysFromFile() throws FileNotFoundException {
    List<Toy> toys = new ArrayList<>();

    try {
      Scanner scanner = new Scanner(new File(filePath));
      while (scanner.hasNextLine()) {
        String line = scanner.nextLine();

        String[] tokens = line.split(";");
        if (tokens.length > 5) {
          System.err.println("Invalid line: " + line);
        } else {
          int id = Integer.parseInt(tokens[0]);
          String name = tokens[1];
          int quantity = Integer.parseInt(tokens[2]);
          String description = tokens[3];
          Toy toy = new Toy(id, name, quantity, description);
          toys.add(toy);
        }
      }
      scanner.close();
    } catch (FileNotFoundException e) {
      System.err.println("File not found: " + filePath);
      throw e;
    }
    return toys;
  }

  // New method to read admin-specific changes from the database
  public List<AdminToy> readAdminChangesFromFile()
    throws FileNotFoundException {
    List<AdminToy> adminToys = new ArrayList<>();
    try {
      Scanner scanner = new Scanner(new File(filePath));
      while (scanner.hasNextLine()) {
        String line = scanner.nextLine();
        String[] tokens = line.split(";");
        if (tokens.length == 5 && tokens[4].equals("ADMIN")) {
          int id = Integer.parseInt(tokens[0]);
          String name = tokens[1];
          int quantity = Integer.parseInt(tokens[2]);
          String description = tokens[3];
          AdminToy adminToy = new AdminToy(id, name, quantity, description);
          adminToys.add(adminToy);
        }
      }
      scanner.close();
    } catch (FileNotFoundException e) {
      System.err.println("File not found: " + filePath);
      throw e;
    }
    return adminToys;
  }

  // New method to update the database with admin-specific changes
  public void updateDatabase(
    List<Toy> toys,
    ToyInterface deletedToy,
    List<AdminToy> adminToys
  ) throws IOException {
    Files.write(Paths.get(getFilePath()), "".getBytes());

    try (
      BufferedWriter writer = new BufferedWriter(new FileWriter(getFilePath()))
    ) {
      // Write non-admin toys excluding the deleted toy
      for (Toy toy : toys) {
        if (!toy.equals(deletedToy) && (deletedToy instanceof Toy)) {
          writer.write(
            String.format(
              "%d;%s;%d;%s%n",
              toy.getId(),
              toy.getName(),
              toy.getQuantity(),
              toy.getDescription()
            )
          );
        }
      }

      for (AdminToy adminToy : adminToys) {
        if (!adminToy.equals(deletedToy) && (deletedToy instanceof AdminToy)) {
          writer.write(
            String.format(
              "%d;%s;%d;%s;ADMIN%n",
              adminToy.getId(),
              adminToy.getName(),
              adminToy.getQuantity(),
              adminToy.getDescription()
            )
          );
        }
      }
    }
  }
}
