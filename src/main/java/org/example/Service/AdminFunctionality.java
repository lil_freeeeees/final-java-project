package main.java.org.example.Service;

import main.java.org.example.Entity.AdminToy;
import main.java.org.example.Entity.Toy;
import main.java.org.example.DaoReader.AdminDaoReader;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class AdminFunctionality {

    private final AdminDaoReader adminDaoReader;

    public AdminFunctionality(String filePath) {
        this.adminDaoReader = new AdminDaoReader(filePath);
    }

    public void updateDetails(int toyId, String newName, int newQuantity, String newDescription) {
        try {
            // Read existing toys from the file
            List<Toy> existingToys = adminDaoReader.readToysFromFile();

            // Find the toy with the specified ID
            Toy existingToy = null;
            for (Toy toy : existingToys) {
                if (toy.getId() == toyId) {
                    existingToy = toy;
                    break;
                }
            }

            // If the toy exists, create an AdminToy with updated details
            if (existingToy != null) {
                AdminToy adminToy = new AdminToy(toyId, newName, newQuantity, newDescription);

                // Update the database with the admin-specific changes
                List<AdminToy> adminToys = new ArrayList<>();
                adminToys.add(adminToy);
                adminDaoReader.updateDatabase(adminToys);

                System.out.println("Admin updated toy details successfully.");
            } else {
                System.out.println("Toy with ID " + toyId + " not found.");
            }
        } catch (IOException e) {
            System.err.println("Error updating details: " + e.getMessage());
        }
    }
}
