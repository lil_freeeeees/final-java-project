package main.java.org.example.Service;

import main.java.org.example.Entity.AdminToy;

import java.util.ArrayList;
import java.util.List;

public interface AdminService {
    public List<AdminToy> adminChanges = new ArrayList<>();

    boolean authenticateAdmin(String username, String password);

    AdminToy addNewToy();

    void removeToy();

    void addNewCategory(String categoryName);

    List<AdminToy> getAdminChanges();
}
