package main.java.org.example.Service;

import main.java.org.example.Entity.Toy;

import java.util.List;

public interface ToySearcher {

    Toy searchById(int id);

    List<Toy> searchByName(String name);


}