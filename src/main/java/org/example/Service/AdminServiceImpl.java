package main.java.org.example.Service;

import java.io.IOException;
import java.util.List;
import java.util.Scanner;
import main.java.org.example.DaoReader.AdminDaoReader;
import main.java.org.example.DaoReader.DaoReaderImpl;
import main.java.org.example.Entity.AdminToy;
import main.java.org.example.Entity.Toy;
import main.java.org.example.Entity.ToyInterface;
import main.java.org.example.util.InputUtils;

public class AdminServiceImpl implements AdminService {

  private final AdminDaoReader adminDaoReader;
  private final DaoReaderImpl daoReaderImpl;
  private final Scanner scanner;

  public AdminServiceImpl(String filePath) {
    this.adminDaoReader = new AdminDaoReader(filePath);
    this.scanner = new Scanner(System.in);
    this.daoReaderImpl = new DaoReaderImpl(filePath);
  }

  @Override
  public boolean authenticateAdmin(String username, String password) {
    return "admin".equals(username) && "123".equals(password);
  }

  @Override
  public AdminToy addNewToy() {
    System.out.println("Enter details for the new toy:");

    List<AdminToy> adminToys = getAdminChanges(); // Assuming getAdminChanges() fetches the toys from the file
    int lastId = adminToys.isEmpty()
      ? 0
      : adminToys.get(adminToys.size() - 1).getId();

    int newId = lastId + 1;

    System.out.print("Name: ");
    String name = scanner.nextLine();

    System.out.print("Quantity: ");
    int quantity = scanner.nextInt();
    scanner.nextLine(); // Consume the newline character

    System.out.print("Description: ");
    String description = scanner.nextLine();

    // Create the new AdminToy object
    AdminToy adminToy = new AdminToy(newId, name, quantity, description);

    // Save the new toy to the file
    try {
      adminDaoReader.addToyToFile(adminToy);
    } catch (IOException e) {
      System.err.println("Error adding toy to the file: " + e.getMessage());
    }

    return adminToy;
  }

  @Override
  public void removeToy() {
    try {
      List<AdminToy> adminToys = getAdminChanges();
      List<Toy> allToys = daoReaderImpl.readToysFromFile(); // Get all toys

      // Display a list of all toys with IDs, names, and quantities
      System.out.println("List of All Toys:");
      for (ToyInterface toy : allToys) {
        System.out.printf(
          "%d. %s (%d left)%n",
          toy.getId(),
          toy.getName(),
          toy.getQuantity()
        );
      }

      // Get user input for the ID of the toy to remove
      System.out.print("Enter the ID of the toy to remove: ");
      int toyIdToRemove = InputUtils.getIntInput();

      // Find the toy with the specified ID
      var toyToRemove = allToys
        .stream()
        .filter(toy -> toy.getId() == toyIdToRemove)
        .findFirst()
        .orElse(null);

      if (toyToRemove != null) {
        // Remove the toy from the adminToys list
        allToys.removeIf(toy -> toy.getId() == toyIdToRemove);

        // Update the database with the modified adminToys list
        daoReaderImpl.updateDatabase(allToys, toyToRemove, adminToys);

        System.out.println("Toy removed successfully!");
        System.out.println("Removed Toy: " + toyIdToRemove);
      } else {
        System.out.println("Toy not found with the specified ID.");
      }
    } catch (IOException e) {
      System.err.println("Error removing toy: " + e.getMessage());
    }
  }

  @Override
  public void addNewCategory(String categoryName) {
    // Implement logic to add a new category (admin-specific changes)
    // For simplicity, do nothing in this example
  }

  @Override
  public List<AdminToy> getAdminChanges() {
    try {
      return adminDaoReader.readAdminChangesFromFile();
    } catch (IOException e) {
      System.err.println("Error reading admin changes: " + e.getMessage());
      return List.of();
    }
  }
}
