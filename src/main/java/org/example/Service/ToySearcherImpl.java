package main.java.org.example.Service;

import main.java.org.example.Entity.AdminToy;
import main.java.org.example.Entity.Toy;

import java.util.ArrayList;
import java.util.List;

public class ToySearcherImpl<T extends Toy> implements ToySearcher {

    private final List<T> toys;

    public ToySearcherImpl(List<T> toys) {
        this.toys = toys;
    }

    @Override
    public Toy searchById(int id) {
        for (T toy : toys) {
            if (toy.getId() == id) {
                return toy;
            }
        }
        return null;
    }

    @Override
    public List<Toy> searchByName(String name) {
        List<Toy> results = new ArrayList<>();
        for (T toy : toys) {
            if (toy.getName().toLowerCase().contains(name.toLowerCase())) {
                results.add(toy);
            }
        }
        return results;
    }
}