package main.java.org.example.Controller;

import main.java.org.example.DaoReader.DaoReaderImpl;
import main.java.org.example.Entity.Toy;
import main.java.org.example.Service.AdminService;
import main.java.org.example.Service.AdminServiceImpl;
import main.java.org.example.Service.ToySearcherImpl;
import main.java.org.example.util.InputUtils;

import java.io.IOException;
import java.util.List;
import java.util.Scanner;

public class ControllerImpl implements Controller {

    private final AdminService adminService;
    private final ToySearcherImpl toySearcherImpl;
    private final DaoReaderImpl daoReaderImpl;

    public ControllerImpl(String toysFilePath) throws IOException {
        this.adminService = new AdminServiceImpl(toysFilePath);
        this.daoReaderImpl = new DaoReaderImpl(toysFilePath);
        List<Toy> toys = daoReaderImpl.readToysFromFile();
        this.toySearcherImpl = new ToySearcherImpl<>(toys);
    }

    // Refactored methods
    public Toy searchById(int id) {
        return toySearcherImpl.searchById(id);
    }

    public List<Toy> searchByName(String name) {
        return toySearcherImpl.searchByName(name);
    }

    @Override
    public void mainController() {
        try {
            Scanner in = new Scanner(System.in);

            boolean running = true;

            while (running) {
                System.out.println("\nPlease select an option:");
                System.out.println("1. Search for toy by ID");
                System.out.println("2. Search for toy by name");
                System.out.println("3. Admin functions");
                System.out.println("4. Developer info");
                System.out.println("5. Quit");

                int choice = in.nextInt();
                in.nextLine();

                switch (choice) {
                    case 1 -> {
                        System.out.print("Enter toy ID: ");
                        int id = in.nextInt();
                        in.nextLine();
                        Toy toy = searchById(id);
                        if (toy != null) {
                            System.out.println("Name: " + toy.getName());
                            System.out.println("Quantity: " + toy.getQuantity());
                            System.out.println("Description: " + toy.getDescription());
                        } else {
                            System.out.println("Toy not found");
                        }
                    }
                    case 2 -> {
                        System.out.print("Enter toy name: ");
                        String name = in.nextLine();
                        List<Toy> results = searchByName(name);
                        if (!results.isEmpty()) {
                            for (Toy toy : results) {
                                System.out.println("Name: " + toy.getName());
                                System.out.println("Quantity: " + toy.getQuantity());
                                System.out.println("Description: " + toy.getDescription());
                                System.out.println();
                            }
                        } else {
                            System.out.println("No toys found with that name");
                        }
                    }
                    case 3 -> adminController(); // New case for admin functions
                    case 4 -> {
                        String projectName = "Toys Warehouse App";
                        String projectDescription = "An App that controls and manages toys' storage";
                        String devName = "Iskandarov Bobomurod";
                        String repoLink = "https://gitlab.com/lil_freeeeees/final-java-project";
                        String createdInDate = "4, December, 2024";
                        String deadLineTillDate = "08, December, 2024";

                        System.out.println("Created And Developed By: " + devName);
                        System.out.println("Project name: " + projectName);
                        System.out.println("Description: " + projectDescription);
                        System.out.println("Link to the repository " + repoLink);
                        System.out.println("The repository created in: " + createdInDate);
                        System.out.println("Deadline: " + deadLineTillDate);
                    }
                    case 5 -> running = false;
                    default -> System.out.println("Invalid choice");
                }
            }
        } catch (Exception e) {
            System.err.println("Error reading file: " + e.getMessage());
        }
    }

    private void adminController() {
        System.out.println("Enter Admin Username:");
        String username = InputUtils.getStringInput();

        System.out.println("Enter Admin Password:");
        String password = InputUtils.getStringInput();

        if (adminService.authenticateAdmin(username, password)) {
            new AdminController(adminService).adminController();
        } else {
            System.out.println("Authentication failed. Exiting admin mode.");
        }
    }
}
