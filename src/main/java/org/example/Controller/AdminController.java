package main.java.org.example.Controller;

import main.java.org.example.Entity.AdminToy;
import main.java.org.example.Service.AdminService;

import java.util.Scanner;

public class AdminController {

    private final AdminService adminService;
    private final Scanner scanner;

    public AdminController(AdminService adminService) {
        this.adminService = adminService;
        this.scanner = new Scanner(System.in);
    }

    public void adminController() {
        System.out.println("Enter Admin Username:");
        String username = scanner.nextLine();

        System.out.println("Enter Admin Password:");
        String password = scanner.nextLine();

        if (adminService.authenticateAdmin(username, password)) {
            boolean adminRunning = true;

            while (adminRunning) {
                System.out.println("\nAdmin Functions:");
                System.out.println("1. Add new toy");
                System.out.println("2. Remove toy");
                System.out.println("3. Add new category");
                System.out.println("4. View admin changes");
                System.out.println("5. Back to main menu");

                int adminChoice = scanner.nextInt();
                scanner.nextLine();

                switch (adminChoice) {
                    case 1:
                        addNewToy();
                        break;
                    case 2:
                        removeToy();
                        break;
                    case 3:
                        addNewCategory();
                        break;
                    case 4:
                        viewAdminChanges();
                        break;
                    case 5:
                        adminRunning = false;
                        break;
                    default:
                        System.out.println("Invalid choice");
                }
            }
        } else {
            System.out.println("Authentication failed. Exiting admin mode.");
        }
    }

    private void addNewToy() {
        AdminToy newAdminToy = adminService.addNewToy();
        System.out.println("New toy added by admin: " + newAdminToy);
    }

    private void removeToy() {
        adminService.removeToy();
    }

    private void addNewCategory() {
        System.out.print("Enter new category name: ");
        String categoryName = scanner.nextLine();
        adminService.addNewCategory(categoryName);
        System.out.println("New category added by admin: " + categoryName);
    }

    private void viewAdminChanges() {
        System.out.println("Admin Changes:");
        adminService.getAdminChanges().forEach(System.out::println);
    }
}
