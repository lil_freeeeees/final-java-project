package main.java.org.example;

import main.java.org.example.Controller.ControllerImpl;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        String toysFilePath = "src/main/java/org/example/resources/toys.txt";

        ControllerImpl controllerImpl = new ControllerImpl(toysFilePath);
        controllerImpl.mainController();
    }
}
