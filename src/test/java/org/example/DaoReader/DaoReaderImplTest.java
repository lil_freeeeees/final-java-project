package test.java.org.example.DaoReader;

import main.java.org.example.DaoReader.DaoReaderImpl;
import main.java.org.example.Entity.Toy;
import org.junit.Before;
import org.junit.Test;

import java.io.FileNotFoundException;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class DaoReaderImplTest {

    private final String FILE_PATH = "src/main/resources/Path/toys.txt";
    private DaoReaderImpl daoReaderImpl;

    @Before
    public void setUp() {
        daoReaderImpl = new DaoReaderImpl(FILE_PATH);
    }

    @Test
    public void testGetSize() throws FileNotFoundException {
        List<Toy> toys = daoReaderImpl.readToysFromFile();

        assertEquals(10, toys.size());
    }

    @Test
    public void testToyNames() throws FileNotFoundException {
        List<Toy> toys = daoReaderImpl.readToysFromFile();

        Toy firstToy = toys.get(0);
        assertEquals("Teddy Bear", firstToy.getName());

        Toy secondToy = toys.get(1);
        assertEquals("LEGO set", secondToy.getName());

        // Add more toy name comparisons as needed...
    }

    @Test
    public void testToyDescriptions() throws FileNotFoundException {
        List<Toy> toys = daoReaderImpl.readToysFromFile();

        Toy firstToy = toys.get(0);
        assertEquals("Soft and cuddly plush toy", firstToy.getDescription());

        Toy secondToy = toys.get(1);
        assertEquals("Build your own adventure", secondToy.getDescription());

        // Add more toy description comparisons as needed...
    }

    @Test
    public void testToyQuantities() throws FileNotFoundException {
        List<Toy> toys = daoReaderImpl.readToysFromFile();

        Toy firstToy = toys.get(0);
        assertEquals(10, firstToy.getQuantity());

        Toy secondToy = toys.get(1);
        assertEquals(5, secondToy.getQuantity());

        // Add more toy quantity comparisons as needed...
    }



}