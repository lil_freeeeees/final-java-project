package main.java.org.example.Entity;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ToyTest {

    @Test
    void testConstructorAndGetters() {
        int id = 1;
        String name = "LEGO set";
        int quantity = 5;
        String description = "Build your own adventure";

        Toy toy = new Toy(id, name, quantity, description);

        assertEquals(id, toy.getId(), "Toy ID should match");
        assertEquals(name, toy.getName(), "Toy name should match");
        assertEquals(quantity, toy.getQuantity(), "Toy quantity should match");
        assertEquals(description, toy.getDescription(), "Toy description should match");
    }
}