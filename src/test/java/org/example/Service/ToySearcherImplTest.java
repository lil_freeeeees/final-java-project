package main.java.org.example.Service;

import main.java.org.example.Entity.Toy;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ToySearcherImplTest {

    private List<Toy> toyList;
    private ToySearcherImpl toySearcherImpl;

    @BeforeEach
    void setUp() {
        toyList = new ArrayList<>();
        toyList.add(new Toy(1, "Teddy Bear", 10, "Soft and cuddly plush toy"));
        toyList.add(new Toy(2, "LEGO set", 5, "Build your own adventure"));
        toyList.add(new Toy(3, "Barbie Doll", 7, "Fashion and fun for kids"));

        toySearcherImpl = new ToySearcherImpl(toyList);
    }

    @Test
    void searchById_validId_returnsToy() {
        int id = 1;
        Toy toy = toySearcherImpl.searchById(id);

        assertNotNull(toy, "Toy should not be null");
        assertEquals(id, toy.getId(), "Toy ID should match");
    }

    @Test
    void searchById_invalidId_returnsNull() {
        int id = -1;
        Toy toy = toySearcherImpl.searchById(id);

        assertNull(toy, "Toy should be null");
    }

    @Test
    void searchByName_validName_returnsListOfToys() {
        String name = "Teddy Bear";
        List<Toy> toys = toySearcherImpl.searchByName(name);

        assertNotNull(toys, "Toys list should not be null");
        assertFalse(toys.isEmpty(), "Toys list should not be empty");
        assertEquals(name, toys.get(0).getName(), "Toy name should match");
    }

    @Test
    void searchByName_invalidName_returnsEmptyList() {
        String name = "InvalidToyName";
        List<Toy> toys = toySearcherImpl.searchByName(name);

        assertNotNull(toys, "Toys list should not be null");
        assertTrue(toys.isEmpty(), "Toys list should be empty");
    }
}