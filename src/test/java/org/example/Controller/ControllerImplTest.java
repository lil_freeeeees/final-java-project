package main.java.org.example.Controller;

import main.java.org.example.Controller.ControllerImpl;
import main.java.org.example.Entity.Toy;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ControllerImplTest {

    private ControllerImpl controller;

    @BeforeEach
    void setUp() throws IOException {
        String toysFilePath = "./src/test/resources/toys.txt"; // Make sure to place the `toys.txt` file in the test resources folder
        controller = new ControllerImpl(toysFilePath);
    }

    @Test
    void searchById_validId_returnsToy() {
        int id = 1;
        Toy toy = controller.searchById(id);

        assertNotNull(toy, "Toy should not be null");
        assertEquals(id, toy.getId(), "Toy ID should match");
    }

    @Test
    void searchById_invalidId_returnsNull() {
        int id = -1;
        Toy toy = controller.searchById(id);

        assertNull(toy, "Toy should be null");
    }

    @Test
    void searchByName_validName_returnsListOfToys() {
        String name = "Teddy Bear";
        List<Toy> toys = controller.searchByName(name);

        assertNotNull(toys, "Toys list should not be null");
        assertFalse(toys.isEmpty(), "Toys list should not be empty");
        assertEquals(name, toys.get(0).getName(), "Toy name should match");
    }

    @Test
    void searchByName_invalidName_returnsEmptyList() {
        String name = "InvalidToyName";
        List<Toy> toys = controller.searchByName(name);

        assertNotNull(toys, "Toys list should not be null");
        assertTrue(toys.isEmpty(), "Toys list should be empty");
    }
}